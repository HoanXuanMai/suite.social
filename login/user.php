<?php
error_reporting(E_ALL);
/**
 * 			SUITE.SOCIAL © 2018 || Social Login
 * ------------------------------------------------------------------------
 * 						** Configuration **
 * ------------------------------------------------------------------------
 */
session_start();
$headline = 'Login into Social Suite Dashboard';
$caption = 'Social Media Management, Marketing, Monitoring & Messaging for your business';
$screen = '//suite.social/images/screen/dashboard.jpg';
$tool = 'Social Suite';
$share_headline = 'Social Suite user';
$share_visitors = '1';
$share_need = '98';
$share_out = '99';
$share_offer = 'FREE Social Media Training!';
$url = 'http://news.suite.social/category/social-media';
$display_networks = '';
$display_whatsapp = '';
$display_email = '';
$display_autoresponders = '';
$button_link = '<a href="//suite.social/dashboard.php" class="btn btn-primary btn-lg btn-block">OR GO TO DASHBOARD <i class="fa fa-arrow-right"></i></a>';

$_SESSION['headline'] = $headline;
$_SESSION['caption'] = $caption;
$_SESSION['screen'] = $screen;
$_SESSION['tool'] = $tool;
$_SESSION['share_headline'] = $share_headline;
$_SESSION['share_visitors'] = $share_visitors;
$_SESSION['share_need'] = $share_need;
$_SESSION['share_out'] = $share_out;
$_SESSION['share_offer'] = $share_offer;
$_SESSION['url'] = $url;
$_SESSION['display_networks'] = $display_networks;
$_SESSION['display_whatsapp'] = $display_whatsapp;
$_SESSION['display_email'] = $display_email;
$_SESSION['display_autoresponders'] = $display_autoresponders;
$_SESSION['button_link'] = $button_link;

header("Location: index.php?r=HOME");
