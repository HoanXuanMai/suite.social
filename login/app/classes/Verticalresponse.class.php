<?php

/**
 * 			SUITE.SOCIAL © 2018 || Social Subscribe
 * ------------------------------------------------------------------------
 * 						** Configuration	**
 * ------------------------------------------------------------------------
 */
require_once("Handling.class.php");

class Verticalresponse {

    public static function get_email() {
        global $Configuration;

        if (isset($_GET['code'])) {
            $user_data = array();
            $token = Handling::curlHttpRequest("https://vrapi.verticalresponse.com/api/v1/oauth/access_token?client_id={$Configuration['verticalresponse_key']}&client_secret={$Configuration['verticalresponse_secret']}&redirect_uri={$Configuration['verticalresponse_callback_url']}&code={$_GET['code']}");
            $token = json_decode($token);
            $user_id = $token->user_id;
            $token = $token->access_token;
            if (!empty($token)) {
            	$user = Handling::curlHttpRequest("https://vrapi.verticalresponse.com/api/v1/contacts?type=standard", "get", [], "", ["Authorization: Bearer $token"]);
            	$user =json_decode($user);
            	if (!empty($user->items)) {
                    $info = end($user->items);
            		$userdata = $info->attributes;
    
            		$resquest_response = json_encode(array("status" => "success", "data" => $userdata, 'contacts' => $user->items));
                	return $resquest_response;
            	}
            }
        }

        #Auth URL
        $url = "https://vrapi.verticalresponse.com/api/v1/oauth/authorize?client_id={$Configuration['verticalresponse_key']}&redirect_uri=" . ($Configuration['verticalresponse_callback_url']);
        return json_encode(array("status" => "url", "data" => array("url" => $url)));
    }

}
