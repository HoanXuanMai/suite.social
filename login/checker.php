<?php

error_reporting(0);
error_reporting(E_ALL);

ob_start();
session_start();

$base_url = "//suite.social/login/user.php";

/// SOCIAL SUITE LOGIN CHECK
if (!isset($_SESSION['dashboard_uid'])) {
    session_destroy(); 
    header("Location: $base_url");
}

echo "Name : ".$_SESSION['name'];
$profile_pic = '//suite.social/src/dist/img/avatar04.png';
if(isset($_SESSION['image']) && !empty($_SESSION['image'])){
    $profile_pic = $_SESSION['image'];
}
?>
<img class="profile-user-img img-responsive img-circle" src="<?php echo $profile_pic; ?>" alt="User profile picture">