/*
This source is shared under the terms of LGPL 3
www.gnu.org/licenses/lgpl.html

You are free to use the code in Commercial or non-commercial projects
*/

 //Set up an associative array
 //The keys represent the number of clients
 //The values represent the cost you charge each client i.e 10 clients is $990
 var cake_prices = new Array();
 cake_prices["10clients"]=990;
 cake_prices["20clients"]=1980;
 cake_prices["30clients"]=2970;
 cake_prices["40clients"]=3960;
 cake_prices["50clients"]=4950;
 cake_prices["60clients"]=5940;
 cake_prices["70clients"]=6930;
 cake_prices["80clients"]=7920;
 cake_prices["90clients"]=8910;
 cake_prices["100clients"]=9900;
 
 //Set up an associative array 
 //The keys represent the months type
 //The value represents the cost of the months i.e. 2 months $198
 //We use this this array when the user selects a filling from the form
 var filling_prices= new Array();
 filling_prices["None"]=0;
 filling_prices["10 Businesses (3 Months)"]=2970;
 filling_prices["10 Businesses (6 Months)"]=5940;
 filling_prices["10 Businesses (12 Months)"]=11880;
 filling_prices["20 Businesses (3 Months)"]=5940;
 filling_prices["20 Businesses (6 Months)"]=11880;
 filling_prices["20 Businesses (12 Months)"]=23760;
 filling_prices["30 Businesses (3 Months)"]=8910;
 filling_prices["30 Businesses (6 Months)"]=17820;
 filling_prices["30 Businesses (12 Months)"]=35640;
 filling_prices["40 Businesses (3 Months)"]=11880;
 filling_prices["40 Businesses (6 Months)"]=23760;
 filling_prices["40 Businesses (12 Months)"]=47520;
 filling_prices["50 Businesses (3 Months)"]=14850;
 filling_prices["50 Businesses (6 Months)"]=29700;
 filling_prices["50 Businesses (12 Months)"]=59400;
 
// getCakeSizePrice() finds the price based on the size of the cake.
// Here, we need to take user's the selection from radio button selection
function getCakeSizePrice()
{  
    var cakeSizePrice=0;
    //Get a reference to the form id="cakeform"
    var theForm = document.forms["cakeform"];
    //Get a reference to the cake the user Chooses name=selectedClient":
    var selectedClient = theForm.elements["selectedclient"];
    //Here since there are 4 radio buttons selectedClient.length = 4
    //We loop through each radio buttons
    for(var i = 0; i < selectedClient.length; i++)
    {
        //if the radio button is checked
        if(selectedClient[i].checked)
        {
            //we set cakeSizePrice to the value of the selected radio button
            //i.e. if the user choose the 8" cake we set it to 25
            //by using the cake_prices array
            //We get the selected Items value
            //For example cake_prices["Round8".value]"
            cakeSizePrice = cake_prices[selectedClient[i].value];
            //If we get a match then we break out of this loop
            //No reason to continue if we get a match
            break;
        }
    }
    //We return the cakeSizePrice
    return cakeSizePrice;
}

//This function finds the filling price based on the 
//drop down selection
function getFillingPrice()
{
    var cakeFillingPrice=0;
    //Get a reference to the form id="cakeform"
    var theForm = document.forms["cakeform"];
    //Get a reference to the select id="filling"
     var selectedFilling = theForm.elements["filling"];
     
    //set cakeFilling Price equal to value user chose
    //For example filling_prices["Lemon".value] would be equal to 5
    cakeFillingPrice = filling_prices[selectedFilling.value];

    //finally we return cakeFillingPrice
    return cakeFillingPrice;
}

//candlesPrice() finds the candles price based on a check box selection
function candlesPrice()
{
    var candlePrice=0;
    //Get a reference to the form id="cakeform"
    var theForm = document.forms["cakeform"];
    //Get a reference to the checkbox id="includecandles"
    var includeCandles = theForm.elements["includecandles"];

    //If they checked the box set candlePrice to 5
    if(includeCandles.checked==true)
    {
        candlePrice=5;
    }
    //finally we return the candlePrice
    return candlePrice;
}

function insciptionPrice()
{
    //This local variable will be used to decide whether or not to charge for the inscription
    //If the user checked the box this value will be 20
    //otherwise it will remain at 0
    var inscriptionPrice=0;
    //Get a refernce to the form id="cakeform"
    var theForm = document.forms["cakeform"];
    //Get a reference to the checkbox id="includeinscription"
    var includeInscription = theForm.elements["includeinscription"];
    //If they checked the box set inscriptionPrice to 20
    if(includeInscription.checked==true){
        inscriptionPrice=20;
    }
    //finally we return the inscriptionPrice
    return inscriptionPrice;
}
        
function calculateTotal()
{
    //Here we get the total price by calling our function
    //Each function returns a number so by calling them we add the values they return together
    var cakePrice = getCakeSizePrice() + getFillingPrice() + candlesPrice() + insciptionPrice();
    
    //display the result
    var divobj = document.getElementById('totalPrice');
    divobj.style.display='block';
    divobj.innerHTML = "Total Amount You Earn: €"+cakePrice;

}

function hideTotal()
{
    var divobj = document.getElementById('totalPrice');
    divobj.style.display='none';
}